const fs = require('fs');
const readLine = require('readline');
const fpath = '/Users/scott/Projects/parkmate/testing_sandbox/bla/config.json';
// // *********************************************************************
// // ***************************** Callbacks *****************************
// // *********************************************************************
// // ES6 style
// fs.readFile('./bla/file.txt', 'utf8', (err, content) => {
//   if (err) throw err;
//   console.log(content);
// });

// // older style
// fs.readFile('./bla/file.txt', 'utf8', function(err, data) {
//     if (err) throw err;
//     console.log(data);
// });

// // ran sequentially...
// function getTotalFileSize(file1, file2, file3, callback) {
//   let total = 0;
//   fs.stat(file1, (error, info) => {
//     total += info.size;
//     fs.stat(file2, (error, info) => {
//       total += info.size;
//       fs.stat(file3, (error, info) => {
//         total += info.size;
//         callback(total);
//         // console.log(total);
//       });
//     });
//   });
// }
// getTotalFileSize('./bla/file.txt','./bla/file.txt', './bla/file.txt', console.log);

// // ran in parallel...
// function getTotalFileSize(file1, file2, file3, callback) {
//   let numFinished = 0;
//   let total = 0;
//   [file1, file2, file3].forEach((file) => {
//     fs.stat(file, (error, info) => {
//       total += info.size;
//       numFinished += 1;
//       if (numFinished === 3) {
//         // console.log(total);
//         callback(total);
//       }
//     });
//   });
// }
// getTotalFileSize('./bla/file.txt','./bla/file.txt', './bla/file.txt', console.log);
// // *********************************************************************

// // *********************************************************************
// // ***************************** Promises ******************************
// // *********************************************************************
// // Promise = an object that represents what the value WILL BE when the operation completes
// // promise constructor takes one argument, a callback with two parameters, resolve and reject.
// // IMPORTANT: .catch == .then(undefined, function(error))
// // With .then(successFunc1, failFunc2), successFunc1 or failFunc2 will be called, never both.
// // But with .then(func1).catch(func2), both will be called if func1 rejects
// // great examples / info: https://developers.google.com/web/fundamentals/primers/promises

// // ======= Example 1 =======
// let promise = new Promise(function(resolve, reject) {
//   // do a thing, possibly async, then…
//   if (1) { resolve("Stuff worked!"); }
//   else { reject(Error("It broke")); }
// });
// promise.then(function(result) {
//   console.log(result); // "Stuff worked!"
// }, function(err) {
//   console.log(err); // Error: "It broke"
// });

// // ES6 style
// let cleanRoom = () => {
//   return new Promise((resolve, reject) => {
//     resolve('cleaned the room');
//   });
// };

// let winCream = (message) => {
//   return new Promise((resolve, reject) => {
//     resolve( message + ' won Cream');
//   });
// };

// cleanRoom()
//   .then((result) => {
//     return winCream(result); // this return allows us to call another .then
//   })
//   .then((result) => {
//     console.log(result);
//   })

// // older style
// let cleanRoom = function() {
//   return new Promise(function(resolve, reject) {
//     resolve('cleaned the room');
//   });
// };

// let winCream = function(message) {
//   return new Promise(function(resolve, reject) {
//     resolve( message + ' won Cream');
//   });
// };
// cleanRoom()
//   .then(function(result) {
//     return winCream(result);
//   })
//   .then(function(result) {
//     console.log(result);
//   })

// // *********************************************************************
// // ***************************** Async / Await *************************
// // *********************************************************************
// // async functions return promises.
// // MUST be a promise after 'await' keyword.
// // you can await other async functions, since async function itself returns a promise
// //
// // ======= Example 1 =======
// let cleanRoom = (arg) => {
//   return new Promise((resolve, reject) => {
//     resolve('cleaned the room ' + arg);
//   });
// };
// async function chores(arg) {
//   let result = await cleanRoom(arg);
//   console.log('waiting...' + result)
// }
// x = chores('bitch');
// console.log(typeof (x));
// console.log(x);

// // ======= Example 2 =======
// function readFile() {
//   return new Promise((resolve, reject) => {
//     fs.readFile(fpath, 'utf8', (err, content) => {
//       if (err) throw err;
//       resolve(content);
//     });
//   });
// };
// async function readConfig() {
//   try {
//     let content = await readFile();
//     let obj = JSON.parse(content.toString());
//     return obj;
//   } catch (error) {
//     // readFile OR JSON.parse error will be caught here
//     console.error('An error occurred', error);
//   }
// }
// readConfig().then((result) => console.log(result));

// // ======= Example 3 =======
// // Example 3 - Promise.all (no working example for this yet)
// let promiseArray = friendIDs.map((id) => {
//   return someAsyncFunction();
// });
// // await Promise.all(promiseArray) is a way to asynchronously wait for all promises to finish
// let finishedPromises = await Promise.all(promiseArray);

// // ======= Example 4 =======
// async function generateRandomNumber() {
//   // simulate an asynchronous operation
//   return new Promise((resolve) => setTimeout(resolve, 1000))  // wait 1 sec, then resolve
//     .then(() => Math.floor(Math.random() * 100));
// }
// async function generateRandomNumber() {
//   // simulate an asynchronous operation
//   // return new Promise(() => disShit());
//   return new Promise((resolve) => resolve(Math.floor(Math.random() * 100)));
//     // .then((content) => content);
// }
// generateRandomNumber().then((result) => console.log(result));
