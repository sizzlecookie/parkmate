import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
  Text,
  Button,
  View,
  StyleSheet,
  Platform,
  PermissionsAndroid
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';

const color = () => Math.floor(255 * Math.random());

/**
 * Sample view to demonstrate StackNavigator
 * @TODO remove this module in a live application.
 */
class ColorView extends Component {
  static displayName = 'ColorView';

  static navigationOptions = {
    title: 'Colors!',
    tabBarIcon: (props) => (
        <Icon name='color-lens' size={24} color={props.tintColor} />
      ),
    // TODO: move this into global config?
    headerTintColor: 'white',
    headerStyle: {
      backgroundColor: '#39babd'
    }
  }

  static propTypes = {
    navigate: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      background: `rgba(${color()},${color()},${color()}, 1)`
    };
  }

  async componentDidMount() {
    try {
      var granted;
      if (Platform.OS === 'android') {
        granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            'title': 'Parkmate App Location Permission',
            'message': 'Parkmate App needs access to your locatino ' +
                      'so we can stalk you and take dick pics.'
          }
        );
      }
      if (Platform.OS !== 'android' || granted === true || granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('We can use your locaiton');
        navigator.geolocation.getCurrentPosition(
            (locationObj) => {
              console.log('We received the locaiton!');
              console.log(locationObj);
              var longitude = locationObj.coords.longitude;
              var latitude = locationObj.coords.latitude;
              var speed = locationObj.coords.speed;
              console.log('Longitude: ' + longitude + '     Latitude ' + latitude);
              this.setState({
                latitude: latitude,
                longitude: longitude,
                speed: speed,
                error: null
              });
            },
            (locationError) => {
              console.log('Error getting location!');
              console.log(locationError);
              this.setState({error: locationError.message});
            });
      } else {
        console.log('Locaiton permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  }

  open = () => {
    this.props.navigate({routeName: 'InfiniteColorStack'});
  };

  render() {
    const buttonText = 'Open in Stack Navigator';
    return (
      <View style={[styles.container, {backgroundColor: this.state.background}]}>
        <Button color='#ee7f06' title={buttonText} onPress={this.open}/>
        <Text>Latitude:{this.state.latitude}</Text>
        <Text>Longitude: {this.state.longitude}</Text>
        <Text>Speed: {this.state.speed}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
});

export default ColorView;
