import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  TouchableOpacity,
  Image,
  Text,
  View,
  TextInput
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';

class CounterView extends Component {
  static displayName = 'CounterView';

  static navigationOptions = {
    title: 'Counter',
    tabBarIcon: (props) => (
        <Icon name='plus-one' size={24} color={props.tintColor} />
      )
  }

  static propTypes = {
    counter: PropTypes.string.isRequired,
    userName: PropTypes.string,
    userProfilePhoto: PropTypes.string,
    loading: PropTypes.bool.isRequired,
    counterStateActions: PropTypes.shape({
      reset: PropTypes.func.isRequired,
      random: PropTypes.func.isRequired,
      saveBluetoothName: PropTypes.func.isRequired
    }).isRequired,
    navigate: PropTypes.func.isRequired,
    bluetoothName: PropTypes.string
  };

  reset = () => {
    this.props.counterStateActions.reset();
  };

  random = () => {
    this.props.counterStateActions.random();
  };

  bored = () => {
    this.props.navigate({routeName: 'Color'});
  };

  renderUserInfo = () => {
    if (!this.props.userName) {
      return null;
    }

    return (
      <View style={styles.userContainer}>
        <Image
          style={styles.userProfilePhoto}
          source={{
            uri: this.props.userProfilePhoto,
            width: 80,
            height: 80
          }}
          />
        <Text style={styles.linkButton}>
          Welcome, {this.props.userName}!
        </Text>
      </View>
    );
  };

  setBluetoothName = (text) => {
    this.setState({bluetoothName: text});
  }

  saveBluetoothName = (bluetoothName) => {
    this.props.counterStateActions.saveBluetoothName(bluetoothName);
  }

  render() {
    const loadingStyle = this.props.loading
      ? {backgroundColor: '#eee'}
      : null;

    return (
      <View style={styles.container}>

        {this.renderUserInfo()}

        <TouchableOpacity
          accessible={true}
          style={[styles.counterButton, loadingStyle]}>
          <Text style={styles.counter}>
            {this.props.counter}
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
            accessible={true}
            accessibilityLabel={'Reset counter'}
            onPress={this.reset}>
          <Text style={styles.linkButton}>
            Reset
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
            accessible={true}
            accessibilityLabel={'Randomize counter'}
            onPress={this.random}>
          <Text style={styles.linkButton}>
            {'Get coordinates\n(example only, doesn\'t save)'}
          </Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={this.bored} accessible={true}>
          <Text style={styles.linkButton}>
            {'I\'m bored!'}
          </Text>
        </TouchableOpacity>

        <TextInput style = {styles.input}
          underlineColorAndroid = 'transparent'
        placeholder = 'btooth name'
          placeholderTextColor = '#9a73ef'
          autoCapitalize = 'none'
          onChangeText = {this.setBluetoothName}/>

        <TouchableOpacity
            style = {styles.submitButton}
            onPress = {
              () => this.saveBluetoothName(this.state.bluetoothName)
            }>
            <Text style = {styles.submitButtonText}> Save </Text>
        </TouchableOpacity>

      </View>
    );
  }
}

const circle = {
  borderWidth: 0,
  borderRadius: 120,
  width: 220,
  height: 220
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white'
  },
  userContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  userProfilePhoto: {
    ...circle,
    alignSelf: 'center'
  },
  counterButton: {
    ...circle,
    backgroundColor: '#349d4a',
    alignItems: 'center',
    justifyContent: 'center',
    margin: 20
  },
  counter: {
    color: 'white',
    fontSize: 20,
    textAlign: 'center'
  },
  welcome: {
    textAlign: 'center',
    color: 'black',
    marginBottom: 5,
    padding: 5
  },
  linkButton: {
    textAlign: 'center',
    color: '#000CCC',
    marginBottom: 10,
    padding: 5
  },
  input: {
    margin: 15,
    height: 40,
    borderColor: '#7a42f4',
    borderWidth: 1
  },
  submitButton: {
    backgroundColor: '#7a42f4',
    padding: 10,
    margin: 15,
    height: 40
  },
  submitButtonText: {
    color: 'white'
  }
});

export default CounterView;
