import {Map} from 'immutable';
import {loop, Effects} from 'redux-loop-symbol-ponyfill';
import {generateRandomNumber} from '../../services/getLocation';
import {asyncSaveBluetooth} from '../../services/saveBluetooth';

// Initial state
const initialState = Map({
  value: 'Save your bluetooth name...\nbitch',
  loading: false
});

// Actions
const RESET = 'CounterState/RESET';
const RANDOM_REQUEST = 'CounterState/RANDOM_REQUEST';
const RANDOM_RESPONSE = 'CounterState/RANDOM_RESPONSE';
const SAVE_BLUETOOTH_REQUEST = 'CounterState/SAVE_BLUETOOTH_REQUEST';
const SAVE_BLUETOOTH_RESPONSE = 'CounterState/SAVE_BLUETOOTH_RESPONSE';

// Action creators
export function reset() {
  return {type: RESET};
}

export function random() {
  return {
    type: RANDOM_REQUEST
  };
}

export async function requestRandomNumber() {
  return {
    type: RANDOM_RESPONSE,
    payload: await generateRandomNumber()
  };
}

export function saveBluetoothName(bluetoothName) {
  return {
    type: SAVE_BLUETOOTH_REQUEST,
    payload: bluetoothName
  };
}

export async function requestSaveBluetoothName(bluetoothName) {
  return {
    type: SAVE_BLUETOOTH_RESPONSE,
    payload: await asyncSaveBluetooth(bluetoothName)
  };
}

// Reducer
export default function CounterStateReducer(state = initialState, action = {}) {
  switch (action.type) {

    case RESET:
      return initialState;

    case RANDOM_REQUEST:
      return loop(
        state.set('loading', true),
        Effects.promise(requestRandomNumber)
      );

    case RANDOM_RESPONSE:
      return state
        .set('loading', false)
        .set('value', action.payload);

    case SAVE_BLUETOOTH_REQUEST:
      return loop(
        state.set('loading', true),
        Effects.promise(requestSaveBluetoothName, action.payload)
      );

    case SAVE_BLUETOOTH_RESPONSE:
      return state
        .set('loading', false)
        .set('value', action.payload);

    default:
      return state;
  }
}
