import {Map} from 'immutable';
import {loop, Effects} from 'redux-loop-symbol-ponyfill';
import {checkBluetoothClassic} from '../../services/checkBluetoothClassic';

// Initial state
const initialState = Map({
  value: 'Check BTConnection...\nbitch',
  loading: false
});

// Actions
const RESET = 'btClassicState/RESET';
const CHECK_BT_CLASSIC_REQUEST = 'btClassicState/SAVE_BLUETOOTH_REQUEST';
const CHECK_BT_CLASSIC_RESPONSE = 'btClassicState/SAVE_BLUETOOTH_RESPONSE';

// Action creators
export function reset() {
  return {type: RESET};
}

export function checkBtClassic() {
  return {
    type: CHECK_BT_CLASSIC_REQUEST
  };
}

export async function requestCheckBtClassic() {
  return {
    type: CHECK_BT_CLASSIC_RESPONSE,
    payload: await checkBluetoothClassic()
  };
}

// Reducer
export default function btClassicStateReducer(state = initialState, action = {}) {
  switch (action.type) {
    case RESET:
      return initialState;

    case CHECK_BT_CLASSIC_REQUEST:
      return loop(
        state.set('loading', true),
        Effects.promise(requestCheckBtClassic)
      );

    case CHECK_BT_CLASSIC_RESPONSE:
      return state
        .set('loading', false)
        .set('value', action.payload);

    default:
      return state;
  }
}
