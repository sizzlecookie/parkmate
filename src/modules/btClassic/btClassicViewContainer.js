import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import btClassicView from './btClassicView';
import {NavigationActions} from 'react-navigation';
import * as btClassicStateActions from '../btClassic/btClassicState';

export default connect(
  state => ({
    btClassic: state.getIn(['btClassic', 'value']),
    loading: state.getIn(['btClassic', 'loading'])
  }),
  dispatch => {
    return {
      navigate: bindActionCreators(NavigationActions.navigate, dispatch),
      btClassicStateActions: bindActionCreators(btClassicStateActions, dispatch)
    };
  }
)(btClassicView);
