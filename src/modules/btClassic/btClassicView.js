import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  View
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';

class btClassicView extends Component {
  static displayName = 'btClassicView';

  static navigationOptions = {
    title: 'BT-Classic',
    tabBarIcon: (props) => (
        <Icon name='plus-one' size={24} color={props.tintColor} />
      )
  }

  static propTypes = {
    btClassic: PropTypes.string.isRequired,
    loading: PropTypes.bool.isRequired,
    btClassicStateActions: PropTypes.shape({
      reset: PropTypes.func.isRequired,
      checkBtClassic: PropTypes.func.isRequired
    }).isRequired
  };

  reset = () => {
    this.props.btClassicStateActions.reset();
  };

  checkBtClassic = () => {
    this.props.btClassicStateActions.checkBtClassic();
  }

  render() {
    const loadingStyle = this.props.loading
      ? {backgroundColor: '#eee'}
      : null;

    return (
      <View style={styles.container}>

        <TouchableOpacity
          accessible={true}
          accessibilityLabel={'Increment btClassic'}
          style={[styles.btClassicButton, loadingStyle]}>
          <Text style={styles.btClassic}>
            {this.props.btClassic}
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
            accessible={true}
            accessibilityLabel={'Reset btClassic'}
            onPress={this.reset}>
          <Text style={styles.linkButton}>
            Reset
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
            style = {styles.submitButton}
            onPress = {
              () => this.checkBtClassic()
            }>
            <Text style = {styles.submitButtonText}> Connected Bitch? </Text>
        </TouchableOpacity>

      </View>
    );
  }
}

const circle = {
  borderWidth: 0,
  borderRadius: 120,
  width: 220,
  height: 220
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white'
  },
  userContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  userProfilePhoto: {
    ...circle,
    alignSelf: 'center'
  },
  btClassicButton: {
    ...circle,
    backgroundColor: '#349d4a',
    alignItems: 'center',
    justifyContent: 'center',
    margin: 20
  },
  btClassic: {
    color: 'white',
    fontSize: 20,
    textAlign: 'center'
  },
  welcome: {
    textAlign: 'center',
    color: 'black',
    marginBottom: 5,
    padding: 5
  },
  linkButton: {
    textAlign: 'center',
    color: '#000CCC',
    marginBottom: 10,
    padding: 5
  },
  input: {
    margin: 15,
    height: 40,
    borderColor: '#7a42f4',
    borderWidth: 1
  },
  submitButton: {
    backgroundColor: '#7a42f4',
    padding: 10,
    margin: 15,
    height: 40
  },
  submitButtonText: {
    color: 'white'
  }
});

export default btClassicView;
