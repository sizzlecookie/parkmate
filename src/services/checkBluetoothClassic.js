import {AsyncStorage} from 'react-native';
import AudioSession from 'react-native-audio-session';
import {NativeEventEmitter, NativeModules} from 'react-native';
const {RNAudioSession} = NativeModules;

async function setSession() {
  try {
    const currentBTName = await AudioSession.availableInputs();
    const userDefinedBTName = await AsyncStorage.getItem('bluetoothName');
    if (currentBTName === 'Not connected' || currentBTName !== userDefinedBTName) {
      console.log(currentBTName);
      // return ('success');
      return ('not connected');
    }
    const calendarManagerEmitter = new NativeEventEmitter(RNAudioSession);
    calendarManagerEmitter.addListener(
      'EventReminder',
      (reminder) => {
        console.log('REMINDER TRIGGERED port: ' + reminder.name);
      }
    );

    return ('Connected to ' + userDefinedBTName);
  }

  catch (error) {
    console.log('ERROR');
    console.log(error);
    return ('error noooo');
  }
}

export async function checkBluetoothClassic() {
  return new Promise((resolve) => resolve(setSession()));
}
