import {
  Platform,
  PermissionsAndroid
} from 'react-native';

function getLocation() {
  return new Promise((resolve, reject) => {
    navigator.geolocation.getCurrentPosition(
      (locationObj) => {
        let long = locationObj.coords.longitude;
        let latitude = locationObj.coords.latitude;
        let speed = locationObj.coords.speed;
        resolve(`long: ${long},\nlat: ${latitude},\nspeed: ${speed}`);
      },
      (locationError) => {
        console.log('Error getting location!');
        console.log(locationError);
        reject(({error: locationError.message}));
      },
      {
        maximumAge: 0,
        enableHighAccuracy: true
      }
    );
  });
}

async function checkPermissions() {
  try {
    var granted;
    if (Platform.OS === 'android') {
      granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
      );
    }
    if (Platform.OS !== 'android' || granted === true || granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log('We can use your location');
      return await getLocation();
    } else {
      console.log('Location permission denied');
      return ('Location permission denied');
    }
  } catch (err) {
    console.warn(err);
    return (err);
  }
}

// original
// export async function generateRandomNumber() {
//   // simulate an asynchronous operation
//   return new Promise((res) => setTimeout(res, 1000))
//     .then(() => Math.floor(Math.random() * 100));
// }

export async function generateRandomNumber() {
  return new Promise((resolve) => resolve(checkPermissions()));
}
