import {AsyncStorage} from 'react-native';

async function getItem() {
  try {
    const value = await AsyncStorage.getItem('bluetoothName');
    if (value !== null) {
      return (value);
    }
  } catch (error) {
    return (error);
  }
}

export async function asyncSaveBluetooth(bluetoothName) {
  return new Promise((resolve, reject) => {
    try {
      AsyncStorage.setItem('bluetoothName', bluetoothName, (err) => {
        if (err) {reject(err);}
        resolve();
      });
    } catch (error) {
      console.log(error);
      console.log('ERROR_bluetooth.js');
      reject(error);
    }
  })
    .then(() => getItem());
}
